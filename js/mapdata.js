var simplemaps_usmap_mapdata={
  main_settings: {
   //General settings
    width: "responsive", //'700' or 'responsive'
    background_color: "#FFFFFF",
    background_transparent: "yes",
    
    //Label defaults
    label_color: "white",
    hide_labels: "no",
    border_color: "#BABABA",
    
    //State defaults
    state_description: "State description",
    state_color: "#CDCCCC",
    state_hover_color: "#000000",
    state_url: "",
    all_states_inactive: "no",
    
    //Location defaults
    location_description: "",
    location_color: "yellow",
    location_opacity: ".8",
    location_url: "",
    location_size: "20",
    location_type: "square",
    all_locations_inactive: "no",
    url_new_tab: "yes",
    auto_load: "yes",
   
    //Zoom settings
    zoom: "no",
    initial_zoom: "-1",
    initial_zoom_solo: "no",
    popups: "off",
    state_image_url: "",
    state_image_position: "",
    all_states_zoomable: "no",
    location_image_url: "",
    label_hover_color: "",
    label_size: "",
    label_font: "",
    div: "map"
  },
  state_specific: {
    HI: {
      name: "Hawaii",
      hide: "no",
      inactive: "yes"
    },
    AK: {
      name: "Alaska",
      hide: "no",
      inactive: "yes"
    },
    FL: {
      name: "Florida",
      inactive: "yes",
      hide: "no"
    },
    NH: {
      name: "New Hampshire",
      hide: "no",
      inactive: "yes"
    },
    VT: {
      name: "Vermont",
      hide: "no",
      inactive: "yes"
    },
    ME: {
      name: "Maine",
      hide: "no",
      inactive: "yes"
    },
    RI: {
      name: "Rhode Island",
      hide: "no",
      inactive: "yes"
    },
    NY: {
      name: "New York",
      hide: "no",
      inactive: "yes"
    },
    PA: {
      name: "Pennsylvania",
      hide: "no",
      inactive: "yes"
    },
    NJ: {
      name: "New Jersey",
      hide: "no",
      inactive: "yes"
    },
    DE: {
      name: "Delaware",
      hide: "no",
      inactive: "yes"
    },
    MD: {
      name: "Maryland",
      hide: "no",
      inactive: "yes"
    },
    VA: {
      name: "Virginia",
      hide: "no",
      inactive: "yes"
    },
    WV: {
      name: "West Virginia",
      hide: "no",
      inactive: "yes"
    },
    OH: {
      name: "Ohio",
      hide: "no",
      inactive: "yes"
    },
    IN: {
      name: "Indiana",
      hide: "no",
      inactive: "yes"
    },
    IL: {
      name: "Illinois",
      hide: "no",
      inactive: "yes"
    },
    CT: {
      name: "Connecticut",
      hide: "no",
      inactive: "yes"
    },
    WI: {
      name: "Wisconsin",
      hide: "no",
      inactive: "yes"
    },
    NC: {
      name: "North Carolina",
      hide: "no",
      inactive: "yes"
    },
    DC: {
      name: "District of Columbia",
      hide: "no",
      inactive: "yes"
    },
    MA: {
      name: "Massachusetts",
      hide: "no",
      inactive: "yes"
    },
    TN: {
      name: "Tennessee",
      hide: "no",
      inactive: "yes"
    },
    AR: {
      name: "Arkansas",
      hide: "no",
      inactive: "yes"
    },
    MO: {
      name: "Missouri",
      hide: "no",
      inactive: "yes"
    },
    GA: {
      name: "Georgia",
      hide: "no",
      inactive: "yes"
    },
    SC: {
      name: "South Carolina",
      hide: "no",
      inactive: "yes"
    },
    KY: {
      name: "Kentucky",
      hide: "no",
      inactive: "yes"
    },
    AL: {
      name: "Alabama",
      hide: "no",
      inactive: "yes"
    },
    LA: {
      name: "Louisiana",
      hide: "no",
      inactive: "yes"
    },
    MS: {
      name: "Mississippi",
      hide: "no",
      inactive: "yes"
    },
    IA: {
      name: "Iowa",
      hide: "no",
      inactive: "yes"
    },
    MN: {
      name: "Minnesota",
      hide: "no",
      inactive: "yes"
    },
    OK: {
      name: "Oklahoma",
      hide: "no",
      inactive: "yes"
    },
    TX: {
      name: "Texas",
      hide: "no",
      inactive: "yes"
    },
    NM: {
      name: "New Mexico",
      hide: "no",
      inactive: "yes"
    },
    KS: {
      name: "Kansas",
      hide: "no",
      inactive: "yes"
    },
    NE: {
      name: "Nebraska",
      hide: "no",
      inactive: "yes"
    },
    SD: {
      name: "South Dakota",
      hide: "no",
      inactive: "yes"
    },
    ND: {
      name: "North Dakota",
      hide: "no",
      inactive: "yes"
    },
    WY: {
      name: "Wyoming",
      hide: "no",
      inactive: "yes"
    },
    MT: {
      name: "Montana",
      hide: "no",
      inactive: "yes"
    },
    CO: {
      name: "Colorado",
      hide: "no",
      inactive: "yes"
    },
    UT: {
      name: "Utah",
      hide: "no",
      inactive: "yes"
    },
    AZ: {
      name: "Arizona",
      hide: "no",
      inactive: "yes"
    },
    NV: {
      name: "Nevada",
      hide: "no",
      inactive: "yes"
    },
    OR: {
      name: "Oregon",
      hide: "no",
      inactive: "yes"
    },
    WA: {
      name: "Washington",
      hide: "no",
      inactive: "yes"
    },
    CA: {
      name: "California",
      hide: "no",
      inactive: "yes"
    },
    MI: {
      name: "Michigan",
      hide: "no",
      inactive: "yes"
    },
    ID: {
      name: "Idaho",
      hide: "no",
      inactive: "yes"
    },
    GU: {
      name: "Guam",
      hide: "yes",
      inactive: "yes"
    },
    VI: {
      name: "Virgin Islands",
      hide: "yes",
      inactive: "yes"
    },
    PR: {
      name: "Puerto Rico",
      hide: "yes",
      inactive: "yes"
    },
    MP: {
      name: "Northern Mariana Islands",
      hide: "yes",
      inactive: "yes"
    }
  },
locations: {
   // "1": {
   //   name: "New York",
   //   lat: 40.7143528,
   //   lng: -74.0059731
   // },
  //  "2": {
   //   name: "Anchorage",
   //   lat: 61.2180556,
   //   lng: -149.9002778,
   //   color: "default"
   // }
  },
  regions: {
    "0": {
      name: "pacific-northwest",
      cascade: "no",
      states: [
        "WA",
        "OR",
        "ID"
      ],
	  zoomable: "no",
      color: "#9f3b49",
      hover_color: "#9f3b49",
    },
    "1": {
      states: [
        "MD",
        "VA",
        "WV",
        "DE",
        "PA",
        "DC"
      ],
      name: "Chesapeake Bay",
      cascade: "no",
	  zoomable: "no",
      color: "#3b729f",
      hover_color: "#3687c9",
      description: ""
    },
    "2": {
      states: [
        "CT",
        "MA",
        "RI",
        "ME",
        "VT",
        "NH"
      ],
      name: "New England",
	  zoomable: "no",	  
      color: "#293c3f",
      hover_color: "#000000",
      description: ""
    },
    "3": {
      states: [
        "TX",
        "LA",
        "MS",
        "AL"
      ],
      name: "Gulf of Mexico",
      color: "#ED9F02",
	  zoomable: "no",	  
      hover_color: "#C28600",
      description: ""
    },
    "4": {
      states: [
        "CA"
      ],
      name: "California",
      color: "#0C6C35",
	  zoomable: "no",	  
      description: "",
      hover_color: "#033E23",
	  state_border_hover_color: "red",
	  state_border_color:"#000000"
    },
    "5": {
      states: [
        "HI"
      ],
      name: "Hawaii",
      color: "#731586",
	  zoomable: "no",
      hover_color: "#500e5d",
      description: ""
    },
	
	    "6": {
      states: [
        "MN",
        "MI",
        "WI",
        "IL"
      ],
      name: "Great Lakes",
      color: "#893912",
	  zoomable: "no",	  
      hover_color: "#754B03",
      description: ""
    }
  },
  continent: [
    {
      x: "56.75740299999999",
      y: "11.111773399999999",
     //General settings
    width: "216.06679700000004", //'700' or 'responsive'
      height: "187.0765266"
    },
    {
      x: "0",
      y: "0",
      height: "615.7894736848264",
      width: "1000"
    }
  ],
  labels: {
	  HI: {
      parent_id: "HI",
      x: 305,
      y: 565,
      pill: "yes"
    },}
};